# Maintainer: Arglebargle < arglebargle AT arglebargle DOT dev >
# Contributor: James McMurray <jamesmcm03@gmail.com>
# Contributor: Roey Darwish Dror <roey.ghost@gmail.com>

# this is a very simple hack of the AUR alma-git package that creates a larger /boot partition

_pkgname="alma"
pkgname="alma-git"
pkgver=r118.6b49b5f+bigboot
pkgrel=1
pkgdesc='Create Arch Linux based live USB'
arch=('x86_64')
url='https://github.com/r-darwish/alma'
license=('GPL3')
makedepends=('git' 'rust')
depends=('gptfdisk' 'parted' 'arch-install-scripts' 'dosfstools' 'coreutils' 'util-linux')
optdepends=('cryptsetup: for root filesystem encryption')
source=(
  "git+https://github.com/r-darwish/${_pkgname}"
  "bigboot.patch"
  )
provides=('alma')
conflicts=('alma')
sha256sums=('SKIP'
            '7ac3bc7f44119755c1bc9a36ff371839485d78c5bb0dcc82b0fa938e692c43b5')

pkgver() {
  cd "${srcdir}/${_pkgname}"
  printf "r%s.%s+bigboot" "$(git rev-list --count HEAD)" "$(git rev-parse --short HEAD)"
}

prepare() {
  cd "${srcdir}/${_pkgname}"
  msg2 "Applying bigboot patch..."
  patch -Np1 -i ../bigboot.patch
}

build() {
  cd "${srcdir}/${_pkgname}"

  cargo build --release
}

package() {
  cd "${srcdir}/${_pkgname}"

  install -Dm755 target/release/${_pkgname} "${pkgdir}/usr/bin/${_pkgname}"
  install -Dm644 LICENSE "${pkgdir}/usr/share/licenses/${_pkgname}/LICENSE"
}
